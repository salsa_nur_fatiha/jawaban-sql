Nama : Salsa Nur Fatiha
NIM : 3202102042
Kelas : MIF 3 B

Soal SQL
Soal 1 MEMBUAT DATABASE
MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.064 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.002 sec)

MariaDB [(none)]> use myshop;
Database changed
MariaDB [myshop]>

Soal 2 MEMBUAT TABEL DALAM DATABASE
MariaDB [myshop]> create table users (
    -> ID int auto_increment primary key,
    -> Name varchar (255),
    -> Email varchar (255),
    -> Password varchar (255)
    -> );
Query OK, 0 rows affected (0.502 sec)

MariaDB [myshop]> create table categories (
    -> ID int auto_increment primary key,
    -> Name varchar (255)
    -> );
Query OK, 0 rows affected (0.274 sec)

MariaDB [myshop]> create table items (
    -> ID int auto_increment primary key,
    -> Name varchar (255),
    -> Description varchar (255),
    -> Price int,
    -> Stock int,
    -> Category_id int,
    -> foreign key (id) references categories (id)
    -> );
Query OK, 0 rows affected (0.269 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.049 sec)

MariaDB [myshop]> desc categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| ID    | int(11)      | NO   | PRI | NULL    | auto_increment |
| Name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.100 sec)

MariaDB [myshop]> desc items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| Name        | varchar(255) | YES  |     | NULL    |                |
| Description | varchar(255) | YES  |     | NULL    |                |
| Price       | int(11)      | YES  |     | NULL    |                |
| Stock       | int(11)      | YES  |     | NULL    |                |
| Category_id | int(11)      | YES  |     | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.056 sec)

MariaDB [myshop]> desc users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| ID       | int(11)      | NO   | PRI | NULL    | auto_increment |
| Name     | varchar(255) | YES  |     | NULL    |                |
| Email    | varchar(255) | YES  |     | NULL    |                |
| Password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.033 sec)

Soal 3 MEMASUKKAN DATA PADA TABEL
insert into users
    -> (Name, Email, Password)
    -> values
    -> ("John Doe","john@doe.com","john123");
Query OK, 1 row affected (0.269 sec)

MariaDB [myshop]> select *from users;
+----+----------+--------------+----------+
| ID | Name     | Email        | Password |
+----+----------+--------------+----------+
|  1 | John Doe | john@doe.com | john123  |
+----+----------+--------------+----------+
1 row in set (0.004 sec)

MariaDB [myshop]> insert into users
    -> (Name, Email, Password)
    -> values
    -> ("Jane Doe","jane@doe.com","jenita123");
Query OK, 1 row affected (0.090 sec)

MariaDB [myshop]> select *from users;
+----+----------+--------------+-----------+
| ID | Name     | Email        | Password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.002 sec)

MariaDB [myshop]> insert into categories
    -> (Name)
    -> values
    -> ('gadget'),
    -> ('cloth'),
    -> ('men'),
    -> ('woman'),
    -> ('branded');
Query OK, 5 rows affected (0.075 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from categories;
+----+---------+
| ID | Name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | woman   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [myshop]> insert into items
    -> (Name, Description, Price, Stock, Category_id)
    -> values
    -> ('Sumsang b50','Hape keren dari merek sumsang','4000000','100','1'),
    -> ('Uniklooh','Baju keren dari brand ternama','5000000','50','2'),
    -> ('IMHO Watch','Jam tangan anak yang jujur banget','2000000','10','1');
Query OK, 3 rows affected (0.079 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| ID | Name        | Description                       | Price   | Stock | Category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | Baju keren dari brand ternama     | 5000000 |    50 |           2 |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

MariaDB [myshop]> update items set Price='500000' where Name='Uniklooh';
Query OK, 1 row affected (0.148 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| ID | Name        | Description                       | Price   | Stock | Category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

Soal 4 MENGAMBIL DATA DARI DATABASE
a. Mengambil Data Users
MariaDB [myshop]> select ID, Name, Email
    -> from users;
+----+----------+--------------+
| ID | Name     | Email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.002 sec)

b. Mengambil Data Items
MariaDB [myshop]> select ID, Name, Description, Price, Stock, Category_id
    -> from items
    -> where price >='1000000';
+----+-------------+-----------------------------------+---------+-------+-------------+
| ID | Name        | Description                       | Price   | Stock | Category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.085 sec)

MariaDB [myshop]> select *from items
    -> where Name like "sang";
Empty set (0.002 sec)

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| ID | Name        | Description                       | Price   | Stock | Category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.002 sec)

c. Menampilkan Data items Join Dengan Kategori
MariaDB [myshop]> select *from items a inner join
    -> categories b
    -> on a.Category_id = b.ID;;
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
| ID | Name        | Description                       | Price   | Stock | Category_id | ID | Name   |
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 2500000 |   100 |           1 |  1 | gadget |
|  2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |  2 | cloth  |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |  1 | gadget |
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
3 rows in set (0.001 sec)

Soal 5 MENGUBAH DATA DARI DATABASE
MariaDB [myshop]>
MariaDB [myshop]> update items set Price='2500000' where Name='Sumsang b50';
Query OK, 1 row affected (0.079 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| ID | Name        | Description                       | Price   | Stock | Category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | Baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | Jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)